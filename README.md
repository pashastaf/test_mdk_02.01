# 3ИСП11-20. Стафеев Павел
<b> Предметная область </b>
<br>
Приложение для склада стройматериалов разработано для оптимизации процессов управления и контроля за инвентарем на нашем строительном складе. Оно включает в себя комплексную базу данных, в которой хранится информация о каждом типе стройматериала, их текущем количестве на складе, а также информацию о поставках, заказах и отгрузках.
- <b>[ER-диаграмма](https://gitlab.com/pashastaf/test_mdk_02.01/-/blob/main/Resources/ERD.png)</b>
![ERD](./Resources/ERD.png)

С помощью этого приложения мы сможем эффективно отслеживать наличие материалов, проводить инвентаризацию, автоматизировать процессы заказов и отгрузок, а также мониторить актуальные потребности в стройматериалах. В таблице вариантов использования описаны основные сценарии использования приложения, включая создание новых заказов, и генерацию отчетов для более точного планирования и управления запасами.
- <b>[Use Case диаграмма](https://gitlab.com/pashastaf/test_mdk_02.01/-/blob/main/Resources/UC.jpg)</b>
![UC](./Resources/UC.jpg)

Это приложение значительно упростит работу на складе, повысив эффективность и снизив риски связанные с нехваткой или избыточными запасами стройматериалов.

- <b>[Физическая модель базы данных](https://gitlab.com/pashastaf/test_mdk_02.01/-/blob/main/Resources/DBPModel.png)</b>

![DBPModel](./Resources/DBPModel.png)

- <b>[SQL Script базы данных](https://gitlab.com/pashastaf/test_mdk_02.01/-/blob/main/BuildShopDB.sql)</b>

- <b>[UI layout](https://www.figma.com/file/JCYCugYJvzf6T9di8iRmmk/Untitled?type=design&node-id=0%3A1&mode=design&t=CEEyZrOURu3beojK-1)</b>
![FigmaDesign](./Resources/FigmaDesign.png)

- <b>[Диаграмма последовательности](https://gitlab.com/pashastaf/test_mdk_02.01/-/blob/main/Resources/UML.jpg)</b>
![UML](./Resources/UML.jpg)

- <b>[Диаграмма активности](https://gitlab.com/pashastaf/test_mdk_02.01/-/blob/main/Resources/activityD.jpg)</b>

![UML](./Resources/activityD.jpg)

- <b>[Диаграмма классов](https://gitlab.com/pashastaf/test_mdk_02.01/-/blob/main/Resources/CD.jpg)</b>

![CD](./Resources/CD.jpg)

- <b>[Проект](https://gitlab.com/pashastaf/test_mdk_02.01/-/tree/project)</b>

</br>
