USE master
DROP DATABASE BuildShopDB

CREATE DATABASE BuildShopDB
USE BuildShopDB

CREATE TABLE employee (
    ID int PRIMARY KEY IDENTITY(1, 1),
    firstName nvarchar(30) NOT NULL,
    secondName nvarchar(30) NOT NULL,
    patronymic nvarchar(30),
    address nvarchar(255) NOT NULL,
    email nvarchar(255) NOT NULL,
    birthday date NOT NULL,
    positionID int NOT NULL,
    photoLink nvarchar(255),
	[login] nvarchar(255),
	[password] nvarchar(255)
);
GO

CREATE TABLE position (
	ID int PRIMARY KEY IDENTITY(1, 1),
	title nvarchar(255) NOT NULL,
	salary decimal(10,2) NOT NULL
)


CREATE TABLE [order] (
    ID int PRIMARY KEY IDENTITY(1, 1),
    saleDate datetime NOT NULL,
    employeeID int NOT NULL,
    discount decimal(1,1) NOT NULL
);
GO


CREATE TABLE product (
    ID int PRIMARY KEY IDENTITY(1, 1),
    title nvarchar(50) NOT NULL,
    categoryID int NOT NULL,
    price decimal(10,2) NOT NULL,
    inStock int NOT NULL,
    photoLink nvarchar(255),
	[description] nvarchar(255)
);
GO

CREATE TABLE orderProduct (
	ID int PRIMARY KEY IDENTITY(1, 1),
    orderID int NOT NULL,
    productID int NOT NULL,
    quantity int NOT NULL
);
GO


CREATE TABLE category (
	ID int PRIMARY KEY IDENTITY(1, 1),
	title nvarchar(255) NOT NULL,
);
GO

CREATE TABLE supplier (
	ID int PRIMARY KEY IDENTITY(1, 1),
	title nvarchar(50) NOT NULL,
	[address] nvarchar(255) NOT NULL,
	phone nvarchar(16) NOT NULL,
	cityID int NOT NULL,
);
GO

CREATE TABLE city (
	ID int PRIMARY KEY IDENTITY(1, 1),
	title nvarchar(50)
)

CREATE TABLE supplyOrder (
	ID int PRIMARY KEY IDENTITY(1, 1),
	orderNum nchar(20) NOT NULL,
	supplierID int NOT NULL,
	productID int NOT NULL,
	productQuantity int NOT NULL,
	orderDate datetime,
	courierID int
)


ALTER TABLE employee
ADD CONSTRAINT FK_employee_position
FOREIGN KEY (positionID) 
REFERENCES position(ID)


ALTER TABLE [order]
ADD CONSTRAINT FK_order_employee
FOREIGN KEY (employeeID) 
REFERENCES employee(ID)

ALTER TABLE orderProduct
ADD CONSTRAINT FK_orderProduct_product
FOREIGN KEY (productID) 
REFERENCES product(ID)

ALTER TABLE orderProduct
ADD CONSTRAINT FK_orderProduct_order
FOREIGN KEY (orderID) 
REFERENCES [order](ID)

ALTER TABLE product
ADD CONSTRAINT FK_product_category
FOREIGN KEY (categoryID) 
REFERENCES category(ID)

ALTER TABLE supplier
ADD CONSTRAINT FK_supplier_city
FOREIGN KEY (cityID) 
REFERENCES city(ID)

ALTER TABLE supplyOrder
ADD CONSTRAINT FK_supplyOrder_product
FOREIGN KEY (productID) 
REFERENCES product(ID)

ALTER TABLE supplyOrder
ADD CONSTRAINT FK_supplyOrder_supplier
FOREIGN KEY (supplierID) 
REFERENCES supplier(ID)


INSERT INTO position 
VALUES
  ('Warehouse worker', 50000),
  ('Seller', 70000),
  ('Administrator', 100000);

INSERT INTO employee (firstName,secondName,patronymic,address,email,birthday,positionID,photoLink)
VALUES
  ('August','Estes','Garza','111-1484 Eu Rd.','august_estes851@mail.ru','16.02.1962',1,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Mechelle','Harper','Middleton','960-2721 Enim. Avenue','mechelle.harper@yandex.ru','29.05.1968',1,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Reece','Vaughan','Bridges','7804 Nunc Rd.','vaughan-reece7976@yandex.ru','31.01.1976',1,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Ciaran','Ashley','Mendez','Ap #175-1658 Sagittis Road','ciaran.ashley4697@mail.ru','30.09.1967',2,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Flavia','Keller','Avila','P.O. Box 256, 4973 Nec, St.','f_keller@yandex.ru','28.01.1971',2,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Bryar','Rowe','Holman','869-7822 Cras St.','b-rowe1302@yandex.ru','14.04.1962',2,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Joshua','Patel','Carver','P.O. Box 687, 8400 Mauris Rd.','pjoshua8611@yandex.ru','24.11.1989',2,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Ashton','Ross','Sutton','Ap #860-7751 At, St.','a_ross1610@mail.ru','16.10.1965',1,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Violet','Jenkins','Johnson','715-1685 Hendrerit Rd.','vjenkins5230@yandex.ru','20.04.1986',3,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Yetta','Shaffer','Downs','6063 Euismod St.','shaffer-yetta7064@yandex.ru','19.04.1968',1,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Jillian','Ingram','Wolfe','254-2080 Adipiscing Avenue','j_ingram7923@mail.ru','30.06.1963',1,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Tate','Bray','Taylor','7500 Semper Rd.','bray.tate@mail.ru','24.09.1977',1,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Yasir','Ochoa','Kramer','5926 Rutrum Avenue','o.yasir3648@mail.ru','31.05.1975',1,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Sharon','Mejia','Pitts','Ap #284-5317 Mauris Rd.','mejia-sharon@mail.ru','14.08.1980',3,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Jasmine','Kinney','Fisher','Ap #709-2560 Pede, Road','kinneyjasmine6951@yandex.ru','20.07.1988',2,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Kelly','Church','Mayo','Ap #982-5924 Penatibus St.','k.church1105@yandex.ru','23.06.1965',2,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Rowan','Shields','Bond','682-2529 Duis Street','r.shields8818@mail.ru','17.03.1986',1,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Jena','Stanton','Frank','960-2922 NOT NULLa. St.','j_stanton@yandex.ru','17.08.1980',2,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Guinevere','Jackson','Potter','264 Volutpat Avenue','jguinevere@yandex.ru','27.01.1969',2,'C:\Users\pasha\source\repos\BuildShop\'),
  ('Amena','Leach','Kirk','Ap #742-7266 At, Avenue','leachamena6249@mail.ru','10.10.1986',2,'C:\Users\pasha\source\repos\BuildShop\');

INSERT INTO [order] (saleDate,employeeID,discount)
VALUES
  ('28.06.2022 14:40:00',16,'0'),
  ('30.05.2022 02:27:46',17,'0.2'),
  ('31.01.2022 09:21:39',19,'0.1'),
  ('08.03.2022 02:56:23',19,'0.1'),
  ('11.01.2023 05:49:58',19,'0'),
  ('11.03.2023 04:10:37',3,'0'),
  ('17.08.2022 04:01:25',8,'0.3'),
  ('01.12.2021 23:27:19',7,'0.3'),
  ('04.01.2023 12:58:07',18,'0.3'),
  ('15.03.2023 16:58:03',4,'0.2'),
  ('01.07.2022 21:49:30',11,'0'),
  ('19.02.2023 10:15:55',2,'0.2'),
  ('16.02.2022 15:04:45',13,'0.1'),
  ('02.06.2022 09:36:58',20,'0.2'),
  ('20.09.2023 07:48:57',9,'0.2'),
  ('31.03.2022 17:44:15',10,'0'),
  ('15.02.2022 09:47:22',10,'0.3'),
  ('03.09.2022 21:57:58',11,'0.1'),
  ('12.11.2021 17:25:36',2,'0.3'),
  ('02.12.2022 11:12:55',8,'0.1'),
  ('14.06.2023 11:41:34',16,'0.1'),
  ('14.02.2022 19:56:21',11,'0.1'),
  ('19.10.2022 17:18:04',18,'0.2'),
  ('01.02.2023 21:41:28',5,'0'),
  ('21.12.2021 23:15:33',5,'0.2'),
  ('10.08.2023 15:15:12',5,'0.3'),
  ('24.02.2023 08:36:46',10,'0.2'),
  ('13.11.2021 15:55:38',20,'0.3'),
  ('02.10.2022 01:06:52',14,'0.3'),
  ('19.06.2022 04:43:14',17,'0.3'),
  ('11.06.2022 00:47:43',7,'0.1'),
  ('28.10.2022 19:03:44',11,'0.3'),
  ('16.01.2022 16:27:29',15,'0.1'),
  ('21.08.2023 12:18:01',4,'0.1'),
  ('22.11.2021 00:59:38',16,'0.3'),
  ('06.02.2022 16:57:07',8,'0'),
  ('22.07.2023 21:36:24',9,'0.2'),
  ('09.05.2022 17:08:52',12,'0'),
  ('08.05.2022 03:04:10',17,'0.1'),
  ('03.05.2023 08:02:35',8,'0.3');

INSERT INTO category 
VALUES
  ('Tools'),
  ('Materials'),
  ('Electrics'),
  ('Interior and decor'),
  ('Protection');


INSERT INTO product (title,categoryID,price,inStock,photoLink,[description])
VALUES
  ('Nails',2,5,123040,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\nails.jpg',''),
  ('Hammer',1,500,1005,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\hammer.jpg',''),
  ('Saw',1,750,900,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\saw.jpg',''),
  ('Bolt',2,10,105000,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\bolt.jpeg',''),
  ('Ruler',1,200,653,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\ruler.png',''),
  ('Bulb',3,150,1053,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\bulb.jpg',''),
  ('Sofa',4,15000,95,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\sofa.jpg',''),
  ('Shovel',1,3500,55,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\shovel.jpg',''),
  ('Garden glove',5,250,662,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\gardenglove.jpg',''),
  ('Protection glove',5,1000,271,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\protectionglove.jpg',''),
  ('Cat 5 Network cabel 10m',3,550,300,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\cat5networkcable10m.jpg',''),
  ('Cat 5 Network cabel 5m',3,350,280,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\cat5networkcable.jpg',''),
  ('Cat 6 Network cabel 10m',3,1550,354,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\cat6networkcable10m.jpg',''),
  ('Cat 6 Network cabel 5m',3,950,234,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\cat6networkcable.jpg',''),
  ('Bar chair',4,5000,53,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\barchair.jpg',''),
  ('Chair',4,2500,209,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\chair.jpg',''),
  ('Table',4,3000,132,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\table.jpg',''),
  ('Paper Block A4 500',2,2500,12500,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\paperblocka4.jpg',''),
  ('Paper Block A5 500',2,1500,1200,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\paperblocka5.jpg',''),
  ('Type-C 2m',3,1500,325,'C:\Users\pasha\source\repos\BuildShop\BuildShop\Res\Images\ProductPhoto\typec2m.jpg','');

  INSERT INTO city
  VALUES
  ('Moscow'),
  ('St.Petersburg'),
  ('Kazan');

  INSERT INTO supplier
  VALUES
  ('Ante Associates','481-2855 Sociis Rd.','+7(253)752-33-85',1),
  ('Quisque Libero Foundation','P.O. Box 334, 2968 Ultricies St.','+7(818)311-40-33',3),
  ('Cursus Et Limited','111-6659 Amet, Road','+7(409)500-45-63',1),
  ('Vitae Semper Egestas Foundation','Ap #948-3217 Id, Road','+7(915)631-31-53',3),
  ('Tempus Corp.','Ap #814-6298 Nec Rd.','+7(409)423-84-36',2),
  ('Scelerisque Neque Nullam Ltd','Ap #239-5434 Purus. Rd.','+7(441)612-21-52',3),
  ('Sit Amet Limited','P.O. Box 270, 1156 Nam Rd.','+7(383)687-15-62',2),
  ('Ante Iaculis Industries','763-3119 Id Street','+7(316)739-12-71',3);